import socket
import threading
import sys
from ttt_game import Game
from time import sleep
import daemon
import daemon.pidfile

game = Game()
currentPlayer = 0
players = 0


def make_info(game_, player):
    sending_str = ""
    for int in game_.make_info(player):
        sending_str = sending_str + str(int) + ","
    # not sending last comma
    return sending_str[:-1]


def threaded_client(conn, player):
    conn.send(str.encode(game.make_info(player)))
    reply = ""
    while True:
        try:
            # how much data we waiting to take [bits]
            player_move = conn.recv(256)
            # we receive encoded info
            reply = game.get_move(int(player_move.decode("utf-8")), player)
            # print("reply", reply)

            conn.send(str.encode(reply))

            if reply[20] == '5' or (reply[20] == '7' and player == 1):
                # time to make sure 2nd player get update from server
                sleep(0.2)
                game.reset()
        except:
            break

    print("Lost connection")
    global currentPlayer
    if player == 1:
        currentPlayer = 1
    else:
        currentPlayer = 0

    global players
    players -= 1

    game.players.remove(player)
    print("players: ", game.players)
    game.reset()

    conn.close()


def main():
    # ----- INIT --------
    server = "192.168.0.157"  # server address
    port = 5555

    # ----- SOCKET INIT -----
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # try to bind
    try:
        s.bind((server, port))
    except socket.error as e:
        print(e)

    # 2 is number of client that can connect
    s.listen(2)

    print("Waiting for 2 players.")

    global players, currentPlayer
    while True:
        print("Started server")
        conn, addr = s.accept()
        print("Connected to:", addr)

        if players <= 1:
            players += 1
            thread = threading.Thread(target=threaded_client, args=(conn, currentPlayer))
            thread.start()
            currentPlayer = 1 - currentPlayer
        else:
            print("Connection closed: ", addr)
            conn.close()
            pass


with daemon.DaemonContext(stdout=sys.stdout, stderr=sys.stderr, pidfile=daemon.pidfile.TimeoutPIDLockFile('/var/run/spam.pid')):
    print("starting main")
    main()
