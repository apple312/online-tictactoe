import socket


class Network:
    def __init__(self):
        self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # self.client.setsockopt(socket.SOL_SOCKET, socket.MSG_NOSIGNAL, 0)
        self.client.settimeout(1)
        self.server = "192.168.0.157"  # server address
        self.port = 5555
        self.addr = (self.server, self.port)
        self.info = self.connect()

    def get_info(self):
        return self.info

    def connect(self):
        try:
            self.client.connect(self.addr)
            return self.client.recv(2048).decode()
        except:
            pass

    def send(self, data):
        try:
            self.client.send(str.encode(data))
            return self.client.recv(2048).decode()

        except socket.error as e:
            print(e)
