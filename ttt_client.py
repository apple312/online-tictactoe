import pygame
from net import Network
from time import sleep

pygame.init()
width = 406
height = 600
win = pygame.display.set_mode((width, height))

pygame.display.set_caption("Client")

white = (255, 255, 255)
green = (100, 100, 100)
blue = (0, 0, 128)
red = (255, 0, 0)
empty_color = (0, 0, 0)


class Text:
    def __init__(self, text, font_size, font_color, x, y):
        self.text = text
        self.x = x
        self.y = y
        self.font_color = font_color
        self.font = pygame.font.Font(None, font_size)

    def draw(self, window):
        text_render = self.font.render(self.text, True, self.font_color, None)
        text_rect = text_render.get_rect()
        text_rect.center = (self.x, self.y)
        window.blit(text_render, text_rect)


class Board:
    def __init__(self, color):
        self.state = [0, 0, 0, 0, 0, 0, 0, 0, 0]
        self.color = color
        self.centers = [[101, 257], [203, 257], [305, 257], [101, 360], [203, 360], [305, 360],
                        [101, 463], [203, 463], [305, 463]]
        self.rect = (52, 202, 302, 302)
        self.x = [[52, 150], [154, 252], [256, 354]]
        self.y = [[202, 300], [304, 402], [406, 504]]

        self.header = 'tic'
        self.message1 = 'init'
        self.message2 = 'init'
        self.main_communicate = 'inicjalize'
        self.font_size = 40
        self.font_color = red
        self.text_x = 100
        self.text_y = 100
        self.t1 = Text(self.header, self.font_size, self.font_color, self.text_x, self.text_y)
        self.t2 = Text(self.message1, self.font_size, self.font_color, self.text_x, self.text_y)
        self.t3 = Text(self.message2, self.font_size, self.font_color, self.text_x, self.text_y)
        self.t4 = Text(self.main_communicate, self.font_size, self.font_color, self.text_x, self.text_y)
        self.b_player = 1
        self.score_1 = 0
        self.score_2 = 0

    def set_text(self, mess, f_s, f_c, t_x, t_y):
        self.font_size = f_s
        self.font_color = f_c
        self.text_x = t_x
        self.text_y = t_y
        t = Text(mess, self.font_size, self.font_color, self.text_x, self.text_y)
        return t

    def draw(self, window):
        pygame.draw.rect(window, self.color, self.rect)
        pygame.draw.rect(window, (0, 0, 0), (150, 202, 4, 302))
        pygame.draw.rect(window, (0, 0, 0), (252, 202, 4, 302))
        pygame.draw.rect(window, (0, 0, 0), (52, 300, 302, 4))
        pygame.draw.rect(window, (0, 0, 0), (52, 402, 302, 4))
        pygame.draw.rect(window, (0, 0, 0), (48, 198, 310, 4))
        pygame.draw.rect(window, (0, 0, 0), (48, 504, 310, 4))
        pygame.draw.rect(window, (0, 0, 0), (48, 198, 4, 310))
        pygame.draw.rect(window, (0, 0, 0), (354, 198, 4, 310))

        self.t1 = self.set_text('TIC TAC TOE', 40, red, 210, 50)
        self.t1.draw(window)
        self.t2 = self.set_text(self.message1, 40, red, 205, 150)
        self.t2.draw(window)
        self.t3 = self.set_text(self.message2, 40, red, 205, 550)
        self.t3.draw(window)
        self.t4 = self.set_text(self.main_communicate, 40, blue, 205, 100)
        self.t4.draw(window)

    def draw_state(self, win):
        for i in range(0, 9):
            if self.state[i] == 1:
                sign = Text("X", 140, red, self.centers[i][0], self.centers[i][1])
            elif self.state[i] == 2:
                sign = Text("O", 140, red, self.centers[i][0], self.centers[i][1])
            else:
                sign = Text("", 140, red, self.centers[i][0], self.centers[i][1])
            sign.draw(win)

    def get_click_position(self):
        if pygame.mouse.get_pressed()[0]:
            position = pygame.mouse.get_pos()
            if 52 <= position[0] <= 150 and 202 <= position[1] <= 300:
                #print("0")
                return 0
            elif 154 <= position[0] <= 252 and 202 <= position[1] <= 300:
                #print("1")
                return 1
            elif 256 <= position[0] <= 354 and 202 <= position[1] <= 300:
                #print("2")
                return 2
            elif 52 <= position[0] <= 150 and 304 <= position[1] <= 402:
                #print("3")
                return 3
            elif 154 <= position[0] <= 252 and 304 <= position[1] <= 402:
                #print("4")
                return 4
            elif 256 <= position[0] <= 354 and 304 <= position[1] <= 402:
                #print("5")
                return 5
            elif 52 <= position[0] <= 150 and 406 <= position[1] <= 504:
                #print("6")
                return 6
            elif 154 <= position[0] <= 252 and 406 <= position[1] <= 504:
                #print("7")
                return 7
            elif 256 <= position[0] <= 354 and 406 <= position[1] <= 504:
                #print("8")
                return 8
            else:
                return 99

    def update(self, info):
        self.state = info[:9]
        self.b_player = info[9]
        self.score_1 = info[11]
        self.score_2 = info[12]

        if info[10] == 0:
            self.main_communicate = 'Waiting for opponent...'
        elif info[10] == 1:
            self.main_communicate = 'Your move!'
        elif info[10] == 2:
            self.main_communicate = 'Opponent moves...'
        elif info[10] == 3:
            self.main_communicate = 'Choose empty cell!'
        elif info[10] == 4:
            self.main_communicate = 'Not your move, wait...'
        elif info[10] == 5:
            self.main_communicate = 'You won! Restart in 3s.'
        elif info[10] == 6:
            self.main_communicate = 'You lost! Restart in 3s.'
        elif info[10] == 7:
            self.main_communicate = 'Draw! Restart in 3s.'

        self.message1 = "SCORE " + str(self.score_1) + " : " + str(self.score_2)

        if self.b_player == 1:
            self.message2 = "You play as 'X'"
        elif self.b_player == 2:
            self.message2 = "You play as 'O'"


def translate_info(str):
    str = str.split(",")
    return_list = []
    for s in str:
        return_list.append(int(s))
    return return_list


def redraw_window(window, board):
    win.fill(white)
    board.draw(window)
    board.draw_state(window)
    pygame.display.update()


def main():
    n = Network()
    try:
        start_info = translate_info(n.get_info())
    except:
        print("Cant connect with server!")
        return 0
    b = Board(green)
    b.update(start_info)

    run = True
    clock = pygame.time.Clock()

    while run:
        clock.tick(60)
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                pos = b.get_click_position()
                while True:
                    try:
                        server_reply = translate_info(n.send(str(pos)))
                        b.update(server_reply)
                        break
                    except:
                        pass

                # you won case
                if server_reply[10] == 5 or server_reply[10] == 7:
                    redraw_window(win, b)
                    sleep(3)
                    pygame.event.clear()  # needed to not get bug

            if event.type == pygame.QUIT:
                run = False
                pygame.quit()

        try:
            server_reply = translate_info(n.send(str(100)))

            # you get known that you lose case
            if server_reply[10] == 6 or server_reply[10] == 7:
                b.update(server_reply)
                redraw_window(win, b)
                sleep(3)
                pygame.event.clear()

            b.update(server_reply)
        except:
            # it is not big problem if not get one update so we can pass
            pass
            # n = Network()
            # print("reconnected")


        redraw_window(win, b)

main()
