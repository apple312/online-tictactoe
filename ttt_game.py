class Game:
    def __init__(self):
        self.state = [0, 0, 0, 0, 0, 0, 0, 0, 0]
        self.player_sign = [1, 2]
        self.player_message = [0, 0]  # player 0 for start get info that your turn, plater 1 gets that should wait for opponent
        self.score = [0, 0]
        self.win = [0, 0]
        # 1 - x moves    2 - o moves
        self.turn = 2  # player 0 starts
        self.players = []

    def check_if_win(self, player):
        if (((self.state[0] == player and self.state[1] == player and self.state[2] == player) == True) or
                ((self.state[3] == player and self.state[4] == player and self.state[5] == player) == True) or
                ((self.state[6] == player and self.state[7] == player and self.state[8] == player) == True) or
                ((self.state[0] == player and self.state[3] == player and self.state[6] == player) == True) or
                ((self.state[1] == player and self.state[4] == player and self.state[7] == player) == True) or
                ((self.state[2] == player and self.state[5] == player and self.state[8] == player) == True) or
                ((self.state[0] == player and self.state[4] == player and self.state[8] == player) == True) or
                ((self.state[2] == player and self.state[4] == player and self.state[6] == player) == True)):
            return 1
        elif 0 not in self.state:
            return 2
        else:
            return 0
            # print('PLAYER ', player, ' WIN')

    def check_if_empty(self, cell):
        if self.state[cell] == 0:
            return 1
        else:
            return 0

    def make_info(self, player):
        rstring = ""
        for cell in self.state:
            rstring += str(cell) + ","

        rstring += str(self.player_sign[player]) + "," + str(self.player_message[player]) + "," \
                   + str(self.score[player]) + "," + str(self.score[1 - player])
        return rstring

    def start_game(self):
        if (self.score[0] + self.score[1]) % 2 == 0:
            self.turn = self.players[0]
            self.player_message[self.players[0]] = 1
            self.player_message[self.players[1]] = 2
        else:
            self.turn = self.players[1]
            self.player_message[self.players[1]] = 1
            self.player_message[self.players[0]] = 2

    def get_move(self, move, player):

        try:
            self.players.index(player)
        except:
            self.players.append(player)
            if len(self.players) == 2:
                self.start_game()

        if move == 100 or move == 99:
            # every loop update ask or out of board click
            return self.make_info(player)

        elif 0 <= move <= 8:
            if self.turn == player:
                if self.check_if_empty(move):
                    # playters get 0 and 1 id and we using 1 as x and 2 as o, so i need to increment it
                    self.state[move] = player + 1
                    self.turn = 1 - self.turn  # toggle turn
                    self.player_message[player] = 2  # msg about wait for opponent
                    self.player_message[1 - player] = 1  # msg about your turn for second player

                    if self.check_if_win(player + 1) == 1:
                        self.player_message[player] = 5  # msg about winning
                        self.score[player] += 1
                        self.player_message[1 - player] = 6  # msg about loosing for other player

                    # draw case
                    elif self.check_if_win(player + 1) == 2:
                        self.player_message[player] = 7  # msg about drawing
                        self.player_message[1 - player] = 7

                    return self.make_info(player)
                else:
                    self.player_message[player] = 3  # msg about you need to choose empty cell or click on board
                    return self.make_info(player)
            else:
                if len(self.players) == 2:
                    self.player_message[player] = 4  # msg about not your move wait for opponent
                return self.make_info(player)

        else:
            #if gets some trash
            return self.make_info(player)

    def reset(self):
        self.state = [0, 0, 0, 0, 0, 0, 0, 0, 0]
        self.player_sign = [1, 2]
        self.player_message = [0, 0]  # player 0 for start get info that your turn, plater 1 gets that should wait for opponent
        self.win = [0, 0]

        if not len(self.players) == 2:
            self.score = [0, 0]
            self.player_message = [0, 0]
            self.turn = 2
            print("players ", self.players)
        else:
            self.start_game()


    #def hard_reset(self):

